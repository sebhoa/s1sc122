"""
embarquement.py

Petit programme de simulation de la phase embarquement :
- vérifier que le passager identifié par sa pièce d'identité fait bien partie de la liste du vol
- et qu'il possède la bonne carte d'embarquement (numéro de siège ok)
"""

import sys

SALLE = 'salle.txt'

def get_enregistrement(vol, delimiter=';'):
    """ouvre le fichier texte dont le nom correspond au numéro vol
    crée un dictionnaire des enregistrements"""
    filename = f'{vol}.csv'
    d_enregistrements = {}
    with open(filename, 'r', encoding='utf-8') as entree:
        for ligne in entree:
            nom, prenom, identite, siege = ligne.strip().split(delimiter)
            d_enregistrements[identite] = {'nom':nom, 'prenom':prenom, 'siege':siege}
    return d_enregistrements


def get_salle(filename, delimiter=';'):
    """Lit le fichier texte correspondant aux paires identité / siege
    et crée le dictionnaire correspondant"""
    d_salle = {}
    with open(filename, 'r', encoding='utf-8') as entree:
        for ligne in entree:
            identite, siege = ligne.strip().split(';')
            d_salle[identite] = siege
    return d_salle

def embarquement(d_enreg, d_salle):
    """la fonction parcourt les paires identite / siege du
    dictionnaire d_salle et supprime l'entrée correspondante
    dans d_enreg si elle existe et que le numero de siege 
    correspond. Retourne True si tout est ok, False sinon."""
    ok_embarquement = True
    for identite, siege in d_salle.items():
        if identite in d_enreg:
            if d_enreg[identite]['siege'] == siege:
                d_enreg.pop(identite)
            else:
                ok_embarquement = False
    return ok_embarquement


# ---
# Programme principal


def main():
    # -- acquisition des données
    #
    numero_vol = sys.argv[1]
    d_enregistrements = get_enregistrement(numero_vol)
    d_salle = get_salle(SALLE)

    # -- traitement
    #
    embarquement_ok = embarquement(d_enregistrements, d_salle)

    # -- restitution des résultats
    #
    if embarquement_ok and d_enregistrements == {}:
        print('OK pour décollage')
    else:
        print('Passagers manquants :')
        print('---------------------')
        for passager in d_enregistrements.values():
            print(f'{passager["nom"]}, {passager["prenom"]}')

if __name__ == "__main__":
    main()
