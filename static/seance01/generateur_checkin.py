"""
génére un fichier csv contenant un certain nombre
de lignes de la forme :
nom;prenom;num_identite;siege
"""

import random
import sys

FICHIER_NOMS = 'noms.txt'
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def generer_sieges(n):
    sieges = [str(num)+lettre for num in range(9, 43) for lettre in 'ABCDEFGHJKL']
    random.shuffle(sieges)
    return sieges[:n]

def generer_noms(n, fichier=FICHIER_NOMS, delimiter=';'):
    with open(fichier, 'r', encoding='utf-8') as entree:
        l_noms = [ligne.strip().split(delimiter) for ligne in entree]
    random.shuffle(l_noms)
    return l_noms[:n]

def un_numero_identite():

    def n_chiffres(n):
        return ''.join(str(random.randint(1,9)) for _ in range(n))

    def n_lettres(n):
        return ''.join(ALPHABET[random.randint(0,25)] for _ in range(n))
    
    return n_chiffres(2) + n_lettres(2) + n_chiffres(5)

def enregister(fichier, noms, identites, sieges):
    with open(fichier, 'w', encoding='utf-8') as sortie:
        for i in range(nb_passagers):
            nom, prenom = noms[i]
            sortie.write(f'{nom};{prenom};{identites[i]};{sieges[i]}\n')

nb_passagers = int(sys.argv[1])
numero_vol = sys.argv[2]
filename = f'{numero_vol}.csv'
l_identites = [un_numero_identite() for _ in range(nb_passagers)]
enregister( filename, 
            generer_noms(nb_passagers), 
            l_identites,
            generer_sieges(nb_passagers))