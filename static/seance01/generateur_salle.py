"""
Genere une salle d'embarquement à partir
d'un numero de vol et donc d'un fichier d'enregistrements

On peut faire manquer des personnes
"""

import random
import sys

SALLE = 'salle.txt'

def lecture(filename, delimiter=';'):
    with open(filename, 'r', encoding='utf-8') as entree:
        return entree.readlines()

def en_salle(l_passagers, nb_manquants, fichier_sortie=SALLE, delimiter=';'):
    with open(fichier_sortie, 'w', encoding='utf-8') as sortie:
        for i in range(nb_manquants, len(l_passagers)):
            _, _, identite, siege = l_passagers[i].strip().split(delimiter)
            sortie.write(f'{identite};{siege}\n')


vol = sys.argv[1]
f_enregistrements = f'{vol}.csv'
nb_manquants = 0
if len(sys.argv) > 2:
    nb_manquants = int(sys.argv[2])
l_passagers = lecture(f_enregistrements)
random.shuffle(l_passagers)
en_salle(l_passagers, nb_manquants)