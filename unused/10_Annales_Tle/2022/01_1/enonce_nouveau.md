Écrire une fonction `nb_occurences` qui prend en paramètres `caractere`, un caractère (ie une chaîne de caractères de longueur 1), et
`mot`, une chaîne de caractères, et qui renvoie le nombre d’occurrences de `caractere`
dans `mot`, c’est-à-dire le nombre de fois où `caractere` apparaît dans `mot`.

Exemples :
```python
>>> nb_occurences('e', "sciences")
2
>>> nb_occurences('i',"mississippi")
4
>>> nb_occurences('a',"mississippi")
0
```