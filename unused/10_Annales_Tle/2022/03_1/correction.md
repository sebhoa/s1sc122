La version attendue est probablement quelque chose comme :

```python
def delta(tab):
    assert len(tab) > 0, "liste vide !"
    tab_compresse = [tab[0]]
    for i in range(1, len(tab)):
        tab_compresse.append(tab[i] - tab[i-1])
    return tab_compresse
```

On peut essayer une version en compréhension :

```python
def delta(tab):
    assert len(tab) > 0, "liste vide !"
    return [tab[0]] + [tab[i] - tab[i-1] for i in range(1, len(tab))]
```

