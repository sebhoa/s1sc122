G. Connan note que :

- ce deuxième exercice est bien trop mathématique, complétant ainsi après la moyenne pondérée un sujet vraiment orienté maths ;
- des libertés encore sur les espaces
- le schéma en triangle des valeurs ne facilite pas la lecture (le traditionnel triangle rectangle aurait  été préférable)

Source : [commentaires à propos du 02.2](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-22)
