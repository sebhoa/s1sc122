G. Connan rappelle qu'il serait bon de choisir le nom des fonctions judicieusement en respectant le [PEP8](https://www.python.org/dev/peps/pep-0008/#naming-conventions). Jeter un oeil à son [commentaire à propos du 05.1](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-51).

En boutade, G. Connan fait évidemment référence à l'[algorithme du _minmax_](https://fr.wikipedia.org/wiki/Algorithme_minimax#:~:text=L'algorithme%20minimax%20(aussi%20appel%C3%A9,dans%20le%20pire%20des%20cas).)

On pourrait alléger la syntaxe des tests et ajouter un troisième exemple où les deux valeurs recherchées sont confondues.