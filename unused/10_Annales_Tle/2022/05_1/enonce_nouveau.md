Écrire une fonction `mini_et_maxi` qui prend en paramètre un tableau de nombres
non triés `tab`, et qui renvoie la plus petite et la plus grande valeur du tableau sous la
forme d’un dictionnaire à deux clés `'min'` et `'max'`. Les tableaux seront représentés sous
forme de liste Python.

Exemples :
```python
>>> mini_et_maxi([0, 1, 4, 2, -2, 9, 3, 1, 7, 1])
{'min': -2, 'max': 9}
>>> mini_et_maxi([42])
{'min': 42, 'max': 42}
>>> min_et_maxi([])
{'min': None, 'max': None}
```