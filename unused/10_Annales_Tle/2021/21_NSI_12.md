# Sujet 12

## Exercice 1

Écrire une fonction `maxi` qui prend en paramètre un tableau `tab` de nombres entiers et qui renvoie un couple donnant le plus grand élément de ce tableau ainsi que l’indice de la première apparition de ce maximum dans le tableau.

**Exemple :**
```python
>>> maxi([1,5,6,9,1,2,3,7,9,8]) 
(9, 3)
```

## Exercice 2

La fonction `recherche` prend en paramètres deux chaînes de caractères `gene` et `seq_adn` et renvoie `True` si on retrouve `gene` dans `seq_adn` et `False` sinon.

Compléter le code Python ci-dessous pour qu’il implémente la fonction `recherche`. 

```python
def recherche(gene, seq_adn):
    n = len(seq_adn)
    g = len(gene)
    i = ...
    trouve = False
    while i < ... and trouve == ... :
        j = 0
        while j < g and gene[j] == seq_adn[i+j]:
            ...
        if j == g:
            trouve = True
        ...
    return trouve
```

**Exemples :**
```python
>>> recherche("AATC", "GTACAAATCTTGCC") 
True
>>> recherche("AGTC", "GTACAAATCTTGCC")
False
```

## Exercice 2 version bis

La fonction `coincide` prend en paramètres deux chaînes de caractères `extrait` et `chaine`, ainsi qu'un entier `i` et retourne `True` si et seulement si la chaîne `extrait` coïncide avec un fragment de la chaîne `chaine` à partir de l'indice `i`.  Voici une définition de cette fonction, que vous devez recopier et compléter :

```python
def coincide(extrait, chaine, i):
    for j in range(len(extrait)):
        if extrait[j] != chaine[i+j]:
            return ...
    ...
```

La fonction `recherche` prend en paramètres deux chaînes de caractères `gene` et `seq_adn` et renvoie `True` si on retrouve `gene` dans `seq_adn` et `False` sinon.

Compléter le code Python ci-dessous pour qu’il implémente la fonction `recherche`, en utilisant la fonction `coincide` :

```python
def recherche(gene, seq_adn):
    n = len(...)
    m = len(...)
    for i in range(n - m + 1):
        if ... :
            return True
    return ...
```
