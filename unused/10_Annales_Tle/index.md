# Epreuves de Terminale

La liste des sujets des épreuves pratiques. Chaque sujet comporte 2 exercices à faire en 1 heure.

## Les modalités de l'épreuve

**Durée : 1 heure**

### Modalités :

- La partie pratique consiste en la résolution de deux exercices sur ordinateur, chacun étant noté sur 4 points.
- Le candidat est évalué sur la base d'un dialogue avec un professeur-examinateur.
- Un examinateur évalue au maximum quatre élèves. L'examinateur ne peut pas évaluer un élève qu'il a eu en classe durant l'année en cours.
- L'évaluation de cette partie se déroule au cours du deuxième trimestre pendant la période de l'épreuve écrite de spécialité.

### Premier exercice

- Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification.
- Il s'agit donc de restituer un algorithme rencontré et travaillé à plusieurs reprises en cours de formation.
- Le sujet peut proposer un jeu de test avec les réponses attendues pour permettre au candidat de vérifier son travail.

### Deuxième exercice

- Pour le second exercice, un programme est fourni au candidat.
- Cet exercice ne demande pas l'écriture complète d'un programme, mais permet de valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.

## Analyse des sujets

??? warning "Les points du programme retirés de l'évaluation"

    1. Histoire de l’informatique
        - Événements clés de l’histoire de l’informatique
    2. Structures de données
        - Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés
    3. Bases de données
        - Système de gestion de bases de données relationnelles
    4. Architectures matérielles, systèmes d’exploitation et réseaux
        - Sécurisation des communications
    5. Langages et programmation
        - Notions de programme en tant que donnée. Calculabilité, décidabilité
        - Paradigmes de programmation
    6. Algorithmique
        - Algorithmes sur les graphes
        - Programmation dynamique
        - Recherche textuelle

??? note "Thèmes du programme offciel"

    - [P1] Représentation des données : types et valeurs de base
        1. représentation binaire des entiers relatifs
        2. représentation approximative des flottants **/!\\ éviter l'égalité de 2 flottants**
        3. valeurs booléennes
        4. manipulation des chaînes de caractères

    - [P2] Représentation des données : types construits
        1. p-uplets, écrire des fonctions retournant un `tuple`
        2. tableaux, y compris en compréhension ici : tableaux mono type et non extensibles
        3. dictionnaires

    - [P3] Traitement des données en tables
        1. tableaux de tableaux, tableaux de p-uplets
        2. recherche dans une table
        3. tri
        4. fusion

    - [P6] Langages et programmation
        1. construction élémentaire
        2. diversité des langages de programmation
        3. spécifications : utilisation d'assertions pour les pré et post conditions d'une fonction
        4. mise au point de programme : utilisation de jeux de tests
        5. utilisation de bibliothèque **/!\\ aucune connaissance exhaustive d'une bibliothèque**

    - [P7] Algorithmique
        1. parcours séquentiel d'un tableau
        2. tris insertion et par sélection ; notion d'invariant
        3. k-plus proches voisins
        4. recherche dichotomique dans un tableau trié
        5. algorithmes gloutons : sac à dos, rendu de monnaie

    - [T1] Structures de données
        1. abstraction : interface ≠ implémentation
        2. vocabulaire de la POO : définir une classe, attributs et méthodes d'instances **/!\\ une erreur dans le programme officiel qui parle d'attributs de classe**
        3. listes, piles, files : structures linéaires ; distinguer les structures par le jeu des méthodes qui les caractérisent
        4. arbres et arbres binaires : structures hiérarchiques

    - [T2] Bases de données
        1. modèle relationnel
        2. base de données relationnelle : distinguer structure et contenu
        3. ~~utilisation d'un SGBD~~
        4. le langage SQL

    - [T4] Langages et programmation
        1. ~~programme comme donnée ; calculabilité, décidabilité~~
        2. récursivité : écrire et analyser un programme récursif
        3. modules : exploiter des modules, en écrire des simples
        4. ~~les différents paradigmes : impératif, fonctionnel et objet~~
        5. mise au point de programme
    
    - [T5] Algorithmique
        1. arbres binaires et arbres binaires de recherche : illustrer la POO, recherche 
        2. ~~algorithmes sur les graphes~~
        3. diviser pour régner : exemples de rotation d'images ou tri fusion
        4. ~~programmation dynamique : alignement de séquences, rendu de monnaie~~
        5. ~~recherche textuelle : algorithme de Boyer-Moore~~