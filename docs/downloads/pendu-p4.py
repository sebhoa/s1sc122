"""
pendu-p4.py

Le code du jeu du pendu (non fini), ce programme
correspond à ce qui normalement a été vu dans les
3 premiers notebooks + l'activité interactions

Le source a été réorganisé pour suivre la structure
d'un vrai programme.

Auteur : Sébastien Hoarau
Date   : 2021.09
"""

# --- Importer le module 

import turtle

# --- Les constantes
MOT = "PYTHON"

# --- Définir les fonctions

def ligne(x1,y1,x2,y2):
    crayon.penup()
    crayon.goto(x1,y1)
    crayon.pendown()
    crayon.goto(x2,y2)

def pendu_1():
    """Les 2 premières barres de la potence"""
    ligne(-200,-150,-100,-150)
    ligne(-150,-150,-150,200)

def pendu_2():
    """Les 2 dernières barres de la potence"""
    ligne(-150, 150, -100, 200)
    ligne(-150, 200, 50, 200)    

def pendu_3():
    """La corde et la tête"""
    crayon.pensize(3)
    crayon.color('black')
    ligne(0, 200, 0, 150)
    crayon.setheading(180)
    crayon.circle(25)

def pendu_4():
    """Le corps"""
    ligne(0, 100, 0, 0)

def pendu_5():
    """Les 2 bras"""
    ligne(0, 80, -50, 50)
    ligne(0, 80, 50, 50)

def pendu_6():
    """Les 2 jambes"""
    ligne(0, 0, -50, -75)
    ligne(0, 0, 50, -75)



# Cette fonction a été réécrite avec un boucle
def carre(x,y):
    crayon.penup()
    crayon.goto(x,y)
    crayon.pendown()
    crayon.setheading(0)
    for _ in range(4):
        crayon.forward(40)
        crayon.left(90)

def carres(n):
    x = -50*n//2
    for i in range(n):
        carre(x,-250)
        x+=50


def affiche_alphabet():
    code_lettre = 65
    abscisse_lettre = -300
    for compteur in range(26):
        crayon.penup()
        crayon.goto(abscisse_lettre, -200)
        crayon.pendown()
        lettre = chr(code_lettre)
        crayon.write(lettre,font=("Arial",16,"bold"),align="center")
        code_lettre += 1
        abscisse_lettre+=25

def barre(lettre):
    abscisse_lettre = -300+(ord(lettre)-65)*25
    crayon.penup()
    crayon.goto(abscisse_lettre-10,-195)
    crayon.color("red")
    crayon.pendown()
    crayon.setheading(45)
    crayon.pensize(3)
    crayon.forward(22)
    crayon.color("black")

# --- Activité 4 : Interactions

def affiche_message(message):
    crayon.penup()
    crayon.goto(0,300)
    crayon.pendown()
    crayon.write(message, font=("Arial",24,"bold"),align="center")

def efface_message():
    crayon.penup()
    crayon.goto(0,300)
    crayon.pendown()
    crayon.color("beige")
    crayon.write(chr(0x2588)*40, font=("Arial",24,"bold"),align="center")
    crayon.color("black")

def get_lettre():
    lettre = feuille.textinput("Proposer une lettre", "Quelle lettre proposez-vous ?")
    if len(lettre) == 1 and lettre in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        efface_message()
        affiche_message("Lettre acceptée")
        return lettre
    else:
        efface_message()
        affiche_message("Il faut saisir une lettre majuscule")
        return ""

def ecrit_lettre(mot, lettre):
    x = -50*len(mot)//2
    for l in mot:
        if l == lettre:
            crayon.penup()
            crayon.goto(x+20,-250)
            crayon.pendown()
            crayon.write(lettre,font=("Arial",24,"bold"),align="center")
        x+=50

def tracer_pendu(nb):
    if nb == 1:
        pendu_1()
    elif nb == 2:
        pendu_2()
    elif nb == 3:
        pendu_3()
    elif nb == 4:
        pendu_4()
    elif nb == 5:
        pendu_5()
    elif nb == 6:
        pendu_6()

# --- Programme principal

# Créer la feuille et le crayon

feuille = turtle.Screen()
crayon = turtle.Turtle()
feuille.bgcolor('beige')
crayon.speed(10)
crayon.hideturtle()

carres(len(MOT))
affiche_alphabet()
# for _ in range(5):
#     lettre = get_lettre()
nb_erreurs = 0
while nb_erreurs < 7:
    lettre = get_lettre()
    # On teste si une lettre a bien été proposée (sinon c'est la chaine vide qui est renvoyée)
    if lettre != "":
        if lettre in MOT:
            ecrit_lettre(MOT, lettre)
        else:
            nb_erreurs+=1
            tracer_pendu(nb_erreurs)

# --- A laisser à la fin

feuille.mainloop()
