## Introduction

La boucle consiste à itérer ou répéter et c'est l'un des concepts fondamentaux de la programmation impérative. Voici des exemples divers de situations courantes où cette notion de répétition est présente :

- Une sportive effectue des mouvements de _cross fit_ et compte : "1... 2... 3... "
- Lucien au CE1 épelle le mot LUCIOLE : "L... U... C... "
- Un parent passe en revue les livres scolaires de son enfant et met à jour une fiche sur l'état des manuels
- Xuan, Mohamed, Benoît et Lucie jouent au Monopoly. Benoît distribue les billets aux joueur-ses.
- Pierre et Hélène jouent au Go

On observe deux types de boucle : la première dite **bornée** coïncide avec les quatre premières situations présentées précédemment et l'autre **non bornée** correspondant au dernier exemple, la partie de Go.

Les deux instructions Python sont `for` (pour la boucle bornée) et `while` pour la boucle non bornée.

## La boucle `for`

### Compter tout simplement

Lorsqu'on souhaite compter, par exemple pour répéter un nombre fixe de fois un bloc d'instructions, on utilise le `for` avec un objet Python qui _distribue_ des entiers : `range`. Par exemple, `range(10)` nous donne les entiers de 0 à 9. Associé au `for` et à une variable, cela permet de compter :

```python linenums="1"
for compteur in range(10):
    # ici les instruction que l'on souhaite répéter
```

Le nom de la variable `compteur` est entièrement libre. Que se passe-t-il ? A la première exécution de l'instruction, `compteur` référence la première valeur entière c'est-à-dire 0. On entre alors dans **le corps de la boucle** c'est-à-dire le bloc d'instructions indentées qu'on exécute une première fois. Puis, on recommence à la ligne 1, en changeant la valeur de `compteur` qui est alors 1 ; et on exécute une 2e fois le corps de la boucle... ainsi de suite pour chacun des entiers jusqu'à 9.

??? note "Note 1" 

    `range` permet de commencer à un autre entier que 0, et on peut compter autrement que de 1 en 1. Pour plus de renseignements consulter la [documentation officielle de `range`](https://docs.python.org/fr/3/library/stdtypes.html#range). 

??? note "Note 2"
     Si vous testez ces boucles dans votre environnement Visual Studio Code ou Thonny, il est possible qu'un _warning_ vous soit exposé, un message du genre :

    > variable compteur not used

    Effectivement, si dans le corps de la boucle vous n'utilisez jamais la variable de votre boucle, alors vous pouvez la remplacer par une variable _anonyme_ : `_` (le souligné ou _underscore_ en anglais) :

    ```python linenums="1"
    for _ in range(5):
        print('hello')
    ``` 

### Énumérer des éléments

Mais la boucle `for`, très très utilisée en Python, sert pour toute sorte d'énumérations. Par exemple, pour aider Lucien à épeler ses mots on pourrait envisager cette fonction (en supposant que `dire` existe et prononce le texte passé en paramètre) :

```python linenums="1"
def epeler(mot):
    for lettre in mot:
        dire(lettre)
```

Nous n'avons pas encore vu les listes mais si `livres` référence un objet de type `list` contenant les livres dont on veut vérifier l'état :

```python linenums="1" 
for livre in livres:
    print(etat(livre))
```

Dans les cas cités ici, la boucle `for` ne fait que parcourir les éléments de la structure (l'**itérable**), sans chercher à modifier ces éléments. Nous verrons avec les structures complexes (listes, dictionnaires) comment se passe les modifications.

## La boucle `while` 

Lorsqu'on ne sait pas quand la boucle doit s'arrêter et qu'il ne s'agit pas d'énumérer des éléments (comme l'exemple de la recherche) alors on utilise la boucle `while` :

- Tant que la condition pour arrêter n'est pas remplie :
    - faire instruction 1
    - faire instruction 2
    - etc.

Par exemple pour la partie de Go entre Pierre et Hélène on aura quelque chose comme :

```python linenums="1"
while fin_de_partie == False:
    # faire jouer le joueur courant
    fin_de_partie = est_ce_fini()
```

Cette boucle nécessite une condition qui soit vraie pour s'arrêter. Cette condition est ce qu'on appelle une **expression booléenne** c'est-à-dire une expression qui vaut `True` ou `False`. Tant que la condition est évaluée à `False` la boucle continue.

Le programme ci-dessous simule des _pierre-feuille-ciseaux_ jusqu'à ce que le joueur 1 (`j1`) gagne 3 fois de suite. La condition porte donc sur la valeur d'une variable `nb_victoire_affilee`. Dès que cette variables atteint 3, le programme s'arrête et affiche le nombre de parties réalisées.

Le programme s'appuie aussi sur une _grosse_ expression booléenne pour savoir si `j1` gagne ou pas. Les constantes du programme aident à comprendre cette expression (qui serait moins parlante si on avait laissé des 0, 1 et 2).

??? exemple "Pierre-Feuille-Ciseaux"
    ```python linenums="1"
    import random

    PIERRE = 0
    FEUILLE = 1
    CISEAUX = 2

    nb_parties = 0
    nb_victoires_affilee = 0
    while nb_victoires_affilee < 3:
        nb_parties += 1
        j1 = random.randint(0,2)
        j2 = random.randint(0,2)
        j1_gagne = j1 == PIERRE and j2 == CISEAUX or j1 == FEUILLE and j2 == PIERRE or j1 == CISEAUX and j2 == FEUILLE 
        if j1_gagne:
            nb_victoires_affilee += 1
        else:
            nb_victoires_affilee = 0
    print(nb_parties)
    ```

### Difficulté didactique du `while`

Dans les cas où elle est vraiment irremplaçable, la boucle `while` est difficile à concevoir correctement. Prenons un exemple simple. Mina explique à Lucie le jeu du pendu :

> Alors je pense à un mot, que je ne te dis pas, et ton but sera de le deviner. Pour ça, je te dessine des carrés à la place de chacune des lettres du mot. Toi tu vas me proposer des lettres et si la lettre fait partie du mot, je la note dans chacun des carrés où elle doit figurer et sinon, je vais réaliser une partie d'un dessin représentant une potence et un pendu... je sais c'est un peu macabre. Tu as un nombre d'erreurs limité, si j'ai dessiné tout le pendu avant que tu ne trouves le mot, tu as perdu. Si tu devines le mot tu gagnes.

Si on décortique cet énoncé, et qu'on ne retient que les informations importantes :

1. on a le but du jeu : deviner un mot caché
2. on a la description de la phase de jeu en deux temps :
    - le joueur qui devine propose une lettre
    - l'autre analyse la réponse et en fonction met à jour soit le dessin du pendu (une erreur), soit l'écriture du mot caché (une bonne réponse)
3. on a la condition de fin de jeu : nombre d'erreurs possibles dépassé (c'est une défaite) ou mot trouvé (c'est une victoire)

La boucle est implicite, on _sent_ bien que la phase principale de jeu (le point 2) va devoir se répéter mais l'énoncé ne commence pas clairement par : _Tant que..._ Et presque dans tous les exemples mettant en oeuvre une boucle `while` vous aurez ce genre de difficulté.

Pour arriver à écrire votre boucle `while`, il faut reprendre la condition de fin, la formaliser (c'est-à-dire réussir à la traduire en une expression booléenne) puis en prendre la négation :

- En supposant que la variable `nb_erreurs` représente le nombre d'erreurs commises et que la variable booléenne `trouve` représente le fait que le mot a été découvert ou pas, la traduction formelle de la condition d'arrêt du jeu est `nb_erreurs > 6 or trouve == True`
- sa négation : `nb_erreurs <= 6 and trouve == False` qui peut aussi s'écrire `nb_erreurs < 7 and not trouve`

Et notre boucle de jeu est alors :

```python
while nb_erreurs < 7 and not trouve:
    lettre = demander_lettre()
    # ici analyser la lettre et mettre à jour ce qu'il faut
    # ...
# fin du jeu :
resultat(trouve)
```

## Des exercices

La section 3.5 du MOOC "Apprendre à coder avec Python" vous propose plusieurs exercices d'application des boucles `for` et `while`.