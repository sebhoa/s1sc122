## Introduction

L'instruction conditionnelle permet, comme son nom l'indique, de réaliser un traitement différencié en fonction de la valeur (booléenne) d'une condition.

Dit plus simplement, il s'agit d'effectuer un bloc d'instructions _B1_ si une expression booléenne vaut _vrai_. Éventuellement, mais c'est facultatif, un bloc _B2_ pourra être réalisé si l'expression est évaluée à _faux_.

## `if`

En Python l'instruction conditionnelle est l'instruction `if` dont la syntaxe est :

```python
if condition:
    # bloc d'instructions
    # B1
    # ...
```

Voici un exemple, un _petit jeu_ simpliste : on énumère les entiers de 1 à un entier donné et on ajoute `FIZZ` sur la ligne suivante si l'entier est multiple de 3 :

```python
def fizz(n):
    for entier in range(1, n+1):
        print(entier)
        if entier % 3 == 0:
            print('FIZZ')
```

!!! exemple "Quelques appels à `fizz`"

    === "exemple 1"

        ```python
        >>> fizz(7)
        1
        2
        3
        FIZZ
        4
        5
        6
        FIZZ
        7
        ```

    === "exemple 2"

        ```python
        >>> fizz(2)
        1
        2
        ```

Dans cette version du `if`, il n'y a pas de traitement particulier si la condition est `False`. On utilise un `else` si on en a besoin :

## `if... else`

Lorsque l'on a un bloc _B2_ à réaliser quand le test est faux, il faut utiliser un `else` ; la syntaxe est alors :

```python
if condition:
    # bloc d'instructions
    # B1
    # ...
else:
    # bloc d'instructions
    # B2
    # ...
```

Reprenons notre jeu _fizz_ et améliorons le un peu : nous allons afficher `FIZZ` si l'entier est multiple de 3 et afficher l'entier sinon.

```python
def fizz_2(n):
    for entier in range(1, n+1):
        if entier % 3 == 0:
            print('FIZZ')
        else:
            print(entier)
```

!!! exemple "Quelques appels à `fizz_2`"

    === "exemple 1"

        ```python
        >>> fizz_2(7)
        1
        2
        FIZZ
        4
        5
        FIZZ
        7
        ```

    === "exemple 2"

        ```python
        >>> fizz_2(2)
        1
        2
        ```

## Imbriquer les `if... else`

Dans les cas où on enchaîne les tests : si... sinon si... sinon si ... le code, avec toutes les indentations peut vite devenir pénible à lire :

```python
if moyenne >= 16:
    mention = 'Très bien'
else:
    if moyenne >= 14:
        mention = 'Bien'
    else:
        if moyenne >= 12:
            mention = 'Assez bien'
        else:
            if moyenne >= 10:
                mention = 'Passable'
            else:
                mention = 'Ajourné.e'
```

Heureusement on peut contracter un `else: if ...:` en `elif ...:` :

```python
if moyenne >= 16:
    mention = 'Très bien'
elif moyenne >= 14:
    mention = 'Bien'
elif moyenne >= 12:
    mention = 'Assez bien'
elif moyenne >= 10:
    mention = 'Passable'
else:
    mention = 'Ajourné.e'
```

### La _vraie_ version du jeu _fizz_

Le jeu décrit précédemment s'appelle en réalité _fizzbuzz_ et consiste à énumérer les entiers et à les remplacer par `FIZZ` lorsqu'ils sont multiples de 3, par `BUZZ` lorsqu'ils sont multiples de 5 et par `FIZZBUZZ` lorsqu'ils sont multiples à la fois de 3 et 5.

!!! exercice "A faire vous-même"

    === "Énoncé"

        En utilisant des `if...elif...`, écrire la définition d'une fonction `fizzbuzz` qui prend un entier en paramètre et réalise les affichages correspondant au jeu _fizzbuzz_.

        Par exemple :

        ```python
        >>> fizzbuzz(16):
        1
        2
        FIZZ
        4
        BUZZ
        FIZZ
        7
        8
        FIZZ
        BUZZ
        11
        FIZZ
        13
        14
        FIZZBUZZ
        16
        ```

    === "Une première solution"    

        ```python
        def fizzbuzz(n):
            for entier in range(1, n+1):
                if entier % 3 == 0 and entier % 5 == 0:
                    print('FIZZBUZZ')
                elif entier % 3 == 0:
                    print('FIZZ')
                elif entier % 5 == 0:
                    print('BUZZ')
                else:
                    print(entier)
        ```

    === "Une autre version"    

        ```python
        def fizzbuzz(n):
            for entier in range(1, n+1):
                message = ''
                if entier % 3 == 0:
                    message += 'FIZZ'
                if entier % 5 == 0:
                    message += 'BUZZ'
                if message == '':
                    message = str(entier)
                print(message)
        ```

## Des exercices

Dans le MOOC "Apprendre à coder avec Python", la section 3.3 offre 8 exercices d'application de l'instruction conditionnelle.