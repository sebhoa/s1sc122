# Introduction

Dans cette rubrique je reprends certaines notions fondamentales de programmation et/ou d'algorithmique, pour les présenter autrement ou plus en détails.

## Bibliographie

Pour aller plus loin, voici deux ouvrages gratuits et téléchargeables en PDF :

- [Manuel complet d'initiation à l'Informatique](https://pixees.fr/un-manuel-complet-dinitiation-a-linformatique/), réalisé par le Learning Lab Inria, la Partie IV concerne le langage Python.
- [Apprendre à programmer avec Python 3](https://inforef.be/swi/download/apprendre_python3_5.pdf), Gérard Swinnen