# Objets, Variables et Affectation

Les programmes manipulent des objets informatiques qui représentent (on dit **modélisent**) les entités de notre problème. 

## Les objets _simples_

- Les entiers relatifs : se sont les objets de type `int` (10, 0, -4, et même des entiers très grands : 2**100)
- Les nombres décimaux : se sont les objets de type `float` (délicats à manipuler pour ce qui est de l'égalité)
- Les booléens : se sont les objets de type `bool` (ils sont 2 : `True` et `False`)
- Les chaînes de caractères : se sont les objets de type `str`. Il s'agit d'une _séquence_ de caractères, encadrée par un délimiteur. Par exemple : `"Bonjour, je programme en Python"`. Le délimiteur ici est le guillemet appelé aussi _double quote_ en anglais. Le délimiteur peut aussi être un _simple quote_ ou aspostrophe en français : `'Voici une autre chaîne de 40 caractères.'`. Pourquoi faut-il des délimiteurs ? Nous répondrons à cette question un peu plus tard.

**Ces objets sont non mutables.** Nous verrons l'importance de ce concept. En français on dit aussi **immuable**.

## Les objets _multiples_

Il permettent de manipuler plusieurs objets via une unique référence. En Python nous verrons :

- Les tableaux qui sont des objets de type `list`
- Les tuples ou n-uplets : des objets de type `tuple`
- Les dictionnaires : des objets de type `dict`
- Les ensembles : des objets de type `set`
- Les fichiers : des objets de type _file_ (en réalité `_io.TextIOWrapper` mais aucune nécessité de se rappeler ce nom barbare)

## Le concept d'affectation

L'idée principale de l'affectation est d'associer un nom _humainement lisible_ à un objet stocké en mémoire et dont notre programme a besoin. La façon dont ce lien est fait dépend de chaque langage de programmation. Il n'est pas important à ce stade de connaître en détail le mécanisme. En Python, il faut retenir que l'affectation a besoin d'un nom (on parle d'**identifiant** ou d'**identificateur**) et de l'objet qui doit être _lié_ à ce nom. Il y a alors une variable qui est créée avec cet identificateur, variable qui va **référencer** l'objet en mémoire ie offrir un moyen (nous n'avons pas besoin de savoir comment exactement) de retrouver l'objet.

Voici une affection à la variable `prenom` à la chaîne de caractères `'Guido'`

```python
>>> prenom = 'Guido'
```

On peut représenter cette affectation graphiquement comme ceci :

```mermaid
graph LR
    A[prenom]-->B('Guido');

    style B fill:#F3DFCF
```

Le schéma ci-dessus s'appelle un **diagramme d'état**. Il s'agit de l'état du programme. L'outil en ligne [pythontutor.com](http://www.pythontutor.com/) permet de visualiser ce diagramme d'état.

Un même objet peut être référencé par plusieurs variables :

```python
>>> prenom = 'Guido'
>>> python_inventeur = prenom
``` 

Lors de la 2e affectation, Python sait très bien que `prenom` n'est pas un objet... il va donc aller chercher l'objet référencé par `prenom` et poser une deuxième référence :

```mermaid
graph LR
    A[prenom]-->B('Guido')
    C[python_inventeur]-->B

    style B fill:#F3DFCF
```

**Un objet doit toujours être référencé pour être utilisé**. Lorsqu'un objet n'est plus référencé, Python le supprime par un mécanisme appelé _garbage collector_ ou _ramasse-miettes_ en français. Ce nettoyage est fait par l'interprète Python et nous n'avons pas à nous en préoccuper.

### Pourquoi faut-il un délimiteur pour l'objet `str` ?

Supposons que les chaînes de caractères n'aient pas besoin de délimiteurs et qu'on veuille un programme qui affiche ceci :

```
prenom : Guido
```

On pourrait imaginer une instruction Python (**attention** il ne s'agit que d'une instruction imaginaire, si vous tentez cela dans un interprète Python vous obtiendrez une erreur) :

```python
>>> print(prenom : Guido)
```

Si on souhaite que le programme fonctionne avec un autre prénom :

```python
>>> prenom = 'Jean'
>>> print(prenom : prenom)
``` 

Comment l'interprète Python peut-il distinguer dans le `print` le `prenom` qui est le texte à écrire donc la chaîne de caractères et le `prenom` qui est la variable référençant une autre chaîne (`Jean` ou `Guido` par exemple) ? **Sans délimiteur c'est impossible.**

**Pourquoi plusieurs délimiteurs ?**

La réponse est dans les exemples suivants :

=== "Le _quote_"

    ```python
    >>> message = 'Ceci est une simple chaîne'
    ```

=== "Le _double quote_"

    La chaîne ci-dessous contient une apostrophe, on ne peut donc pas l'utiliser comme délimiteur.

    ```python
    >>> message = "Python c'est un langage"
    ```

=== "Le _double quote_ triple"

    Le _quote_ triple ou le _double quote_ triple permet des chaînes de caractères sur plusieurs lignes. On s'en sert pour les _docstrings_ ces commentaires particuliers qui donnent l'aide d'une fonction ou d'un programme.

    Le message ci-dessous contient à la fois une apostrophe et des guillements, on ne peut donc utiliser ni l'un ni l'autre comme délimiteur.

    ```python
    >>> message = """L'enseignant : "Bonjour"
    ... -- les étudiants : "Bonjour"
    ... -- ..."""
    ```

=== "Neutraliser un caractère spécial"

    L'apostrophe `'` et le guillemet `"` sont donc pour Python des caractères spéciaux : ils servent de délimiteurs de chaînes de caractères lorsqu'ils sont par 2. Si on a besoin de ces deux caractères dans une chaîne, on peut neutraliser la fonction spécial pour que Python considère que c'est un simple caractère et non un délimiteur.

    On utilise pour cela le caractère _anti-slash_ `\` ; on dit qu'on _échappe le caractère spécial_ comme ceci :

    ```python
    >>> message = 'L\'enseignant dit : "Bonjour"'
    ```

    ou 

    ```python
    >>> message = "L'enseignant dit : \"Bonjour\""
    ```


### Supprimer une référence

Une variable  peut cesser de référencer un objet :

```python
>>> x = 1
```
La variable `x` référence l'objet `1`.

```mermaid
graph LR
    A[x]-->B(1);

    style B fill:#F3DFCF
```
Si on **incrémente** _x_ :

```python
>>> x = x + 1
```

Un nouvel objet est créé par l'addition entre le `1` référencé par `x` et le `1` de l'expression arithmétique pour former l'objet `2`. Le `1` n'est alors plus référencé par `x` qui référence `2`... comme le `1` n'avait pas d'autres références, il sera détruit au bout d'un certain temps.


```mermaid
graph LR
    B(1);
    A[x]-->C(2);

    style B fill:#F3DFCF;
    style C fill:#F3DFCF;
```

???+ "Exercice"

    Faites le diagramme d'état des affectations suivantes :

    ```python
    >>> x = 10
    >>> y = x
    >>> z = 2*y
    ```

??? success "Python tutor"

    ![diagramme0](../assets/images/diag_var.png)


???+ success "Diagramme _à la main_"
    
    ```mermaid
    graph LR
    X[x]-->B(10);
    Y[y]-->B;
    Z[z]-->C(20);

    style B fill:#F3DFCF;
    style C fill:#F3DFCF;
    ```

### Peut savoir si 2 variables références le même objet ?

Il y a un cas simple : celui où on effectue explicitement le référencement au même objet :

```python
>>> var1 = 100
>>> var2 = var1
```

Ici clairement `var1` et `var2` référencent le **même objet** puisque c'est exactement ce qu'on voulait.

Mais si on fait :

```python
>>> var1 = 100
>>> var2 = 100
```

S'agit-il du même objet en mémoire ? La réponse est simple : ce qui se passe **réellement** en mémoire en s'en moque, pour nous, ce sont **deux objets différents** ; ayant certes la même _valeur_ mais différents. C'est notamment essentiel pour les listes et les autres objets multiples mutables. Ainsi :

```python
>>> liste1 = [0, 1, 2, 3]
>>> liste2 = [0, 1, 2, 3]
```

Nos deux variables références bien deux listes différentes et modifier une des listes ne touchera pas à l'autre (et c'est heureux). Lorsqu'un objet est créé, Python lui attribue un **numéro d'identification**, on peut connaître la valeur de ce numéro par la fonction prédéfinie `id`.

Par exemple :

```python
>>> a = 1
>>> id(a)
2458524543280
```

Si vous faites cet essai dans votre interprète python vous obtiendrez probablement un autre numéro.

```python
>>> msg = 'Ceci est un objet'
>>> alias = msg
>>> id(msg)
2458598033008
>>> id(msg) == id(alias)
True
```

??? note "Exercice"

    1. Construire la liste des couples `[(-9, -9), (-8, -8), ... (259, 259), (260, 260)]` 
    2. Afficher les valeurs de `a` pour les couples `(a, b)` de cette liste tels que `id(a) != id(b)`
    3. Qu'en déduisez-vous ?  

## Le cas des fonctions

Quel est le rapport entre les fonctions et l'affectation ? Lorsque vous définissez une fonction **avec des paramètres**, au moment de l'appel avec des arguments effectifs, il y a des affectations implicites entre les variables que sont les paramètres et les arguments de l'appel.

??? example "Un premier exemple"

    La fonction `ligne` qui permet de tracer une trait entre deux positions grâce au module `turtle` :

    ```python
    from turtle import *
    
    def ligne(x1, y1, x2, y2):
        penup()
        goto(x1, y1)
        pendown()
        goto(x2, y2)
    ```

    Que se passe-t-il lorsqu'on utilise cette fonction avec des valeurs ? Par exemple avec l'appel suivant :

    ```python
    >>> ligne(100, 120, 200, -300)
    ```

    Les objets `100`, `120`, `200` et `-300` sont créés, et référencés par les variables `x1`, `y1` etc. respectivement. Attention les variables créées ne sont "visibles" que dans la fonction `ligne` et détruites à la fin de celle-ci.

??? example "Autre exemple"

    Voici un autre exemple et le diagramme d'état généré dans pythontutor :

    ```python
    def somme(a, b):
        return a + b
    
    print(somme(10, 20))
    ```

    ![diagramme](../assets/images/somme.png){: .centrer}

    Après l'exécution du `return` les variables `a` et `b` sont détruites. Pour vous en convaincre, testez ce code en rajoutant l'instruction `print(a)` **après** le `print(somme(a, b))`.

    ([lien vers pythontutor de l'exemple](https://pythontutor.com/visualize.html#code=def%20somme%28a,%20b%29%3A%0A%20%20%20%20return%20a%20%2B%20b%0A%0Aprint%28somme%2810,%2020%29%29&cumulative=false&curInstr=3&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false))






## Le cas des `list`

L'objet `list` est un tableau dynamique. Quelque soit le langage de programmation, un tableau est caractérisé par des cases mémoires contigues (côte à côte) car la principale caractéristique d'un tableau c'est d'offrir un mécanisme d'accès rapide via un indice entier (0, 1 etc.).

Ainsi un tableau `T` qu'il possède quelques éléments ou plusieurs millions, offrira le même temps d'accès à n'importe lequel de ses éléments : par l'opérateur crochet ; `T[i]` où `i` est un entier entre 0 et n-1 (`n` étant le nombre d'éléments du tableau).

Sans entrer trop dans les détails, cela est possible parce que les éléments d'un tableau sont tous du même type. Ainsi, connaissant l'adresse en mémoire du premier élément, il est facile de trouver l'adresse du i<sup>e</sup> si toutes les cases mémoires ont la même taille : `adresse_i = adresse_0 + i * taille_une_case`. Certains langages de programmation comme le langage C offre la possibilité de manipuler explicitement les adresses appelées _pointeurs_. **Ce n'est pas le cas en Python**.

Mais, les listes de Python peuvent contenir des objets de types différents. En réalité on a à faire à **un tableau de références** et la variable référence ce tableau. Comme ceci :

```python
>>> ma_liste = [10, 'hello', True, 8.3]
```


Le diagramme associé :

![diagramme](../assets/images/diag_liste.png){: .centrer}

Toutes les cases jaunes de notre tableau sont des références vers des objets. La liste est un **objet mutable** cela signifie qu'on peut changer les références du tableau _jaune_. Les références sont les objets du tableau ; pour accéder au premier objet on utilise la notation crochet : `ma_liste[0]` représente donc la première référence, vers l'objet `10`. Changeons cette référence (en créant un _alias_ vers l'objet 10 pour ne pas le perdre):

```python
>>> alias = ma_liste[0]
>>> ma_liste[0] = 0
```

![diagramme2](../assets/images/diag_liste2.png){: .centrer}


## Le `tuple` 

Il s'agit aussi d'un tableau de références mais la différence c'est que les références ne sont pas modifiables : la `tuple` est un objet **non mutable**.

```python
>>> point = (-1, 0, 2)
>>> point[0] = 1
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-2-bb39a8d0d324> in <module>
----> 1 point[0] = 1
TypeError: 'tuple' object does not support item assignment
```

![diagramme3](../assets/images/diag_tuple.png){: .centrer}


=== "Exercice"

    Faites le diagramme d'état des affectations suivantes :

    ```python
    >>> prenoms = ['jean', 'lucie', 'aline']
    >>> prenoms2 = prenoms
    >>> prenoms2[0] = 'jeanne'
    ```


=== "Réponse"

    ![diagramme0](../assets/images/diag_prenoms.png){: .centrer}

    !!! warning "Attention"

        Notez comme il n'y a qu'une seule liste référencée par 2 variables (`prenoms` et `prenoms2`) et que les éléments peuvent être modifiés via l'une ou l'autre des références.  


## Parcourir une liste

Que se passe-t-il lorsqu'on parcourt une liste ?

Si notre liste de `n` éléments est référencée par `tab`, on peut parcourir directement les références `tab[0]`, ..., `tab[n-1]`. Supposons que `tab` soit une liste d'entiers et qu'on veuille la parcourir pour l'afficher en colonne :

```python
for i in range(n):
    print(tab[i])
```

On peut aussi se servir d'une autre référence, appelons la `valeur` qui va référencer chacun leur tour les entiers de la liste :

```python
for valeur in tab:
    print(valeur)
```

Et voici l'évolution du diagramme d'état (utiliser les flèches pour avancer et reculer) :

???+ note "Diagramme d'état du parcours"

    <object data="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20print%28valeur%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=0&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false" width="800" height="500">
        <embed src="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20print%28valeur%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=0&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </embed>
        <a href="http://pythontutor.com/visualize.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20print%28valeur%29%0A&cumulative=false&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false">Suivez le lien si rien ne s'affiche dans la frame</a>
    </object>

???+ note "Exercice"

    Supposons qu'on veuille multiplier par 10 chacun des entiers de la liste `tab` précédente. Que parcours utiliseriez-vous ? Codez votre solution dans python tutor et visualisez l'exécution. Conclure.


??? fail "Mauvaise idée"

    Le parcours par la référence _tierce_ est une mauvaise idée :

    <object data="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20valeur%20%3D%20valeur%20*%2010%0Aprint%28tab%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=1&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false" width="800" height="500"> 
        <embed src="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20valeur%20%3D%20valeur%20*%2010%0Aprint%28tab%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=1&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </embed>
        <a href="http://pythontutor.com/visualize.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20valeur%20in%20tab%3A%0A%20%20%20%20valeur%20%3D%20valeur%20*%2010%0Aprint%28tab%29%0A&cumulative=false&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false">Suivez le lien si rien ne s'affiche dans la frame</a>
    </object>

??? success "Solution"

    Pour modifier les valeurs d'une liste il faut parcourir par les indices :

    <object data="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20i%20in%20range%28len%28tab%29%29%3A%0A%20%20%20%20tab%5Bi%5D%20%3D%20tab%5Bi%5D%20*%2010%0Aprint%28tab%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=1&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"  width="800" height="500">
        <embed src="http://pythontutor.com/iframe-embed.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20i%20in%20range%28len%28tab%29%29%3A%0A%20%20%20%20tab%5Bi%5D%20%3D%20tab%5Bi%5D%20*%2010%0Aprint%28tab%29&codeDivHeight=400&codeDivWidth=350&cumulative=true&curInstr=1&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </embed>
        <a href="http://pythontutor.com/visualize.html#code=tab%20%3D%20%5B5,%201,%203,%202,%204%5D%0Afor%20i%20in%20range%28len%28tab%29%29%3A%0A%20%20%20%20tab%5Bi%5D%20%3D%20tab%5Bi%5D%20*%2010%0Aprint%28tab%29%0A&cumulative=false&curInstr=0&heapPrimitives=true&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false">Suivez le lien si rien ne s'affiche dans la frame</a>
    </object>