## Des fonctions pour factoriser du code

Dans l'activité du jeu du pendu avec turtle, nous avons utilisé des fonctions pour regrouper des instructions souvent répétées ensembles pour la réalisation d'une tâche. C'est ce qu'on appelle **factoriser** dans le jargon de la programmation. Ce genre de fonctions s'appellent des [procédures](https://fr.wikipedia.org/wiki/Programmation_proc%C3%A9durale) dans certains langages de programmation. Pas en Python. 

Ainsi, réaliser une ligne entre deux points consiste à :

- lever le crayon
- aller au premier point
- abaisser le crayon 
- aller au second point

Dans cette séquence de quatre instructions, il y a deux paramètres : les points entre lesquels on souhaite tracer la ligne (ou 4 si on considère séparément chacune des coordonnées). En Python, voici comment définir une telle fonction :

```python linenums="1"
def ligne(x1, y1, x2, y2):
    crayon.penup()
    crayon.goto(x1, y1)
    crayon.pendown()
    crayon.goto(x2, y2)
```

Les **paramètres** de la fonction sont des variables locales qui vont venir référencer les **arguments** passés lors des appels à la fonction :

```python
>>> ligne(-200,-150,-100,-150)
>>> ligne(-150,-150,-150,200)
```

Au moment du premier appel, `x1` est affecté à -200, `y1` à -150 etc. Ces variables sont détruites à la fin de la fonction.


## Des fonctions pour calculer

Il existe aussi (le plus souvent) des fonctions qui calculent un résultat et qui mettent ce résultat à disposition du reste du programme par l'instruction `return`.

Dans le jeu du pendu, notre programme demande au joueur de saisir une lettre. La fonction du module `turtle` utilisé est `textinput` qui ouvre une petite fenêtre de dialogue dans laquelle l'utilisateur peut saisir un texte avant de valider. Voici le résultat obtenu dans un environnement Linux Ubuntu pour l'appel suivant (`feuille` étant le nom donné à l'écran de turtle):

```python
>>> feuille.textinput('Proposer une lettre', 'Quelle lettre proposez-vous ?')
```

![textinput fenêtre](../assets/images/textinput.png){: .centrer}

Dans l'exemple de notre jeu (voir le chapitre [Jeu du Pendu](../../08_Jeu_du_pendu/interactions/)) nous devons récupérer ce que l'utilisateur aura saisi et le référencer par une variable ; de plus, comme il nus faudra effectuer quelque vérifications sur la saisie (qu'il s'agit bien d'une seule lettre majuscule), nous décidons d'en faire une fonction :

```python
def demander_lettre():
    saisie = feuille.textinput('Proposer une lettre', 'Quelle lettre proposez-vous ?')
    # ici les instructions pour les vérifications de la saisie
    return saisie
```

C'est la dernière instruction `return saisie` qui nous permet d'utiliser notre fonction comme ceci :

```python
lettre = demander_lettre()
```

Si à la place de `return` nous utilisons un `print`, certes la saisie serait affichée à l'écran mais notre variable `lettre` ne référencerait rien du tout (c'est-à-dire l'objet très particulier `None`). La suite du programme du pendu ne pourrait pas se faire.

!!! exercice "A faire vous-même"

    Entraînez-vous à détecter les fonctions qui nécessitent un `return` et celles qui n'en n'ont pas besoin. Dans le jeu du pendu il y a les deux.

    Utilisez votre interprète interactif et/ou pythontutor pour bien comprendre la différence entre une fonction avec `return` et une fonction sans.



