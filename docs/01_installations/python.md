# Python

![Logo Python](../assets/images/logo_python.svg){align=left} ![Guido van Rossum](../assets/images/guido_van_rossum.jpg){align=right} Le premier outil à installer est un _interprète Python3_ qui est le langage que nous allons utiliser pour cette initiation. [Python](https://www.python.org/) est un langage _multi-paradigmes_ (nous reviendrons sur ce terme) inventé par [Guido van Rossum](https://fr.wikipedia.org/wiki/Guido_van_Rossum), informaticien néerlandais.

## A quoi ça sert ?

Python3 est un langage interprété c'est-à-dire que pour exécuter un programme écrit en Python3, il faut un autre programme qui va lire les instructions d'un fichier appelé code source (un fichier au format texte .py) et les exécuter dans l'ordre. Cet autre programme s'appelle un **interprète**.


## Installation de Python


=== "Linux / OSX"

    Rien de spécial à faire : python est nativement installé sur ces systèmes. Pour vérifier cela ouvrez un Terminal et taper la commande suivante :
    ``` bash
    python3
    ```

    Vous devriez obtenir quelque chose de similaire à :
    
    ```
    Python 3.11.1 (v3.11.1:a7a450f84a, Dec  6 2022, 15:24:06) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin Type "help", "copyright", "credits" or "license" for more information.
    >>> 
    ```

    Si ce n'est pas le cas, essayez la commande :
    ``` bash
    python
    ```

    
=== "Windows"

    Si vous avez déjà installé un environnement de type _conda_ ([miniconda](https://doc.ubuntu-fr.org/miniconda) ou [Anaconda](https://www.anaconda.com/products/individual)) il est probable que Python soit déjà installé. Référez-vous alors à votre environnement pour lancer un interprète Python.
    
    Dans le cas contraire, rendez-vous sur le site officiel de Python : [www.python.org/downloads](https://www.python.org/downloads) et cliquez sur le bouton jaune qui, normalement vous permettra de télécharger et installer la bonne version en fonction de votre système d'exploitation.
    
    !!! warning "Attention" 
        Veuillez à bien cocher les deux petites cases en bas d'une des premières fenêtres de la procédure d'installation :

        ![cocher](../assets/images/install_python_windows.png)

--- 

!!! warning "Attention"
    Quelque soit votre système d'exploitation, votre version de Python doit être **3.x** et non 2.x ! Au moment où ce document est rédigé la dernière version est 3.11.


## Installation de modules supplémentaires

L'installation de modules se fera en utilisant l'outil `pip` si vous n'êtes pas sous un environnement _conda_ ou par l'outil `conda` sinon.

Les modules intéressants que nous allons probablement utiliser :

### `turtle`

Ce module reprend le langage Logo pour la réalisation de petits dessins.

- Installation : `pip3 install turtle`
- Site et doc : [documentation officielle](https://docs.python.org/3/library/turtle.html)
- Tester le module en lançant un interprète interactif et taper :

    ```python
    >>> import turtle
    >>> help(turtle)
    ```

### `svg_turtle`

Le [langage SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics) permet de réaliser des graphiques décrits par des équations mathématiques et donc étirables et rétractables à l'envie sans provoquer de phénomène de pixellisation comme avec les formats d'images basés sur les pixels (jpg, png etc.)

Le module `svg_turtle` permet de faire du _turtle_ en python mais d'obtenir la sortie au format svg dans un fichier plutôt qu'une sortie visuelle à l'écran.

- Installation : `pip3 install svg_turtle`
- Utilisation : s'utilise comme le module `turtle` (ce sont les mêmes instructions pour dessiner) à l'exception de la création de la _tortue_ pour dessiner et de la sauvegarde du dessin dans un fichier svg
- Pour tester, créer un programme qui commence par charger l'objet `SvgTurtle` :
    ```python
    from svg_turtle import SvgTurtle
    t = SvgTurtle()
    t.fd(200)
    t.save_as('test.svg')
    ```
<!--
![dessin_svg](/assets/images/coral5x5.svg){ width="300px" }
-->

### `ipythonblocks`

Ce module permet de manipuler des grilles de cellules colorées. Il s'utilise **uniquement** dans un Notebook Jupyter.

- Installation : `pip3 install ipythonblocks`
- Pour tester : lancer votre serveur jupyterlab ([voir la section concernée](/01_installations/jupyter/)) et dans une cellule de code :
    ```python
    from ipythonblocks import BlockGrid
    ```
    ```python
    grille = BlockGrid(20, 30)
    grille.show()
    ```
- Pour se familiariser avec `ipythonblocks` : **attention** le [site officiel](http://www.ipythonblocks.org/about) ne semble plus fonctionner ; il faut dire que le projet est assez ancien. Nous verrons les principales fonctionnalités en TD si nous nous servons de l'outil.