# MOOC

Un MOOC est un cours ouvert (ie accessible au monde entier), en ligne : l'acronyme est anglais et signifie _Massively Open Online Course_

Créé en partenariat avec l'ULB (Université Libre de Bruxelles), le MOOC _Apprendre à Coder avec Python_ est proposé sur la plateforme fun-mooc depuis 2019.

Il couvre les concepts de bases de la programmation dite impérative en utilisant le langage Python3 comme support.

Dans cette partie vous allez apprendre à vous inscrire à un mooc sur la plateforme fun-mooc.

## A quoi ça sert ?

Le MOOC est un cours en ligne complet et structuré, sous forme de vidéos, de petits quiz pour vous tester et surtout d'exercices auto-corrigés avec l'outil UpyLaB. Il vous sert donc d'outil pour apprendre, réviser, échanger (avec les autres apprenants).

## Évaluation

Un quiz est prévu pour valider que vous avez effectivement réalisé le travail de cette partie : l'inscription et la visualisation des capsules vidéos.

## Inscription

![accueil FUN](../assets/images/mooc01.png){ width="300px"; align=right }

1. Rendez-vous sur [fun-mooc.fr](https://www.fun-mooc.fr)...
2. Vous créer un compte si vous n'en possédez pas. Vous pouvez utiliser votre numéro étudiant comme _userid_
3. Rechercher le cours _Apprendre à coder avec Python_ et vous inscrire. 