# UpyLaB

Il s'agit de l'exerciseur (on dit aussi _juge en ligne_) qui vous permet de réaliser les exercices Python dans le Mooc. Il existe une plateforme que nous utiliserons pour les exercices à préparer pour les séances. Un suivi peut-être fait plus facilement que dans le Mooc. Ce suivi permettra de vous donner une note de contrôle continu.

## Inscription

1. L'inscription à la plateforme : rendez-vous sur [upylab2.ulb.ac.be/](https://upylab2.ulb.ac.be)
2. Créer un compte **étudiant** en utilisant votre numéro d'étudiant comme pseudo
3. Le lien d'invitation est disponible sur le moodle du cours