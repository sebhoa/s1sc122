![logo](assets/images/logo_rosace.svg) 

# Méthodologie Scientifique : les 6h d'Informatique

## Les Objectifs 

### 1. Se familiariser avec l'organisation

Il s'agit de vous familiariser avec l'organisation générale des UEs de programmation et notamment de voir l'articulation entre les différents éléments :

=== "moodle"

    C'est le point d'entrée de l'organisation. La plateforme de l'Université sert à vous donner les informations importantes, à mettre les liens vers les différents éléments, à faire les QCM, à vous permettre de déposer vos devoirs etc.

=== "les pages gitlab.io"

    Il s'agit d'un dépôt gitlab de pages markdown tranformées automatiquement en pages HTML statiques. Ces pages contiendront des compléments de cours, d'explications, des résumés etc. Comme la page que vous lisez actuellement.

=== "le mooc"

    Un MOOC pour débutants qui initie à la programmation impérative en utilisant Python3.

=== "les notebooks jupyter"

    Ce sont des pages contenant à la fois du texte en markdown, à la fois du code Python exécutable. Ces pages sont accessibles dans un navigateur une fois le serveur lancé.

=== "Autres petits outils"

    - [CodinGame](hhtps://www.codingame.com) est une plateforme de challenges de programmation en ligne. Nous utiliserons l'outil _Clash of Code_ (petite compétition à 12 participants). Il vous faudra vous inscrire à cette plateforme.
    - [WooClap](https://www.wooclap.com/fr/) un outil de questionnaires que nous utiliserons parfois en CM pour dynamiser certains moments.


### 2. Installer les outils

Il s'agit d'**installer les outils** nécessaires aux activités de programmation :

    - un interprète Python
    - un éditeur de texte
    - un serveur jupyter lab

### 3. Apprendre les premières briques de programmation

Il s'agit d'apprendre ou de réviser les premières instructions Python3 via la **résolution de petits problèmes** 

### 4. Acquérir plus d'autonomie

Pour la suite de vos apprentissages il est primordial d'être autonome sur :

- les façons d'apprendre
- la recherche d'informations
- la gestion de vos outils numériques

## Différents outils

![organisation](assets/images/orga_info.svg){: .centrer}

## Communication

Voici les différents moyens de communiquer avec l'enseignant, en fonction du type de la demande :

=== "contenu pédagogique du cours"

    1. Directement pendant la séance **si la question est en lien avec le contenu de la séance**
    2. A la fin de la séance si la question n'est pas en lien avec le contenu de la séance, venir voir directement l'enseignant
    3. Poser la question sur le forum moodle du cours

=== "Aspect technique du cours (EDT, contrôles etc.)"

    1. A la fin de la séance, venir voir directement l'enseignant
    2. Poser la question sur le forum moodle du cours ou de la filière s'il s'agit d'une question liée à la filière **L1 Informatique**

=== "Personnelle"

    Par mail ou via un entretien privé sur la plateforme zoom (prendre un rdv au préalable)

    !!! warning "Attention"
    
        Tout mail doit être correctement formaté :

        - Un _bonjour_
        - Rappel du **numéro étudiant**, du **nom**, du **prénom** et de la **filière** (cela peut être fait dans la signature du mail)
        - Être précis sur la demande : une question de cours ? une question technique d'organisation ? une réclamation ? 
        - Rester poli, notamment ne pas **exiger** un traitement immédiat de votre demande 
