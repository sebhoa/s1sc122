# Séance 2

**mardi 13.09** 10h15--12h15<br>
**mercredi 14.09** 8h--10h

1. QCM : _Vérifier la préparation_ (10')
2. _Brainstorming_ : Quels concepts de la programmation a-t-on vu ?
3. Retour sur l'affectation
4. Retour sur les fonctions
5. Retour sur les boucles
6. Questions-Réponses
